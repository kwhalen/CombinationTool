#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ class TOwnedList+ ;
#pragma link C++ class ExtendedMinimizer+ ;
#pragma link C++ class ParametrisationScheme+ ;
#pragma link C++ class ParametrisationSequence+ ;
#pragma link C++ class RenamingMap+ ;
#pragma link C++ class CorrelationScheme+ ;
#pragma link C++ class Channel+ ;
#pragma link C++ class AbsMeasurement+ ;
#pragma link C++ class Measurement+ ;
#pragma link C++ class CombinedMeasurement+ ;

#endif
