#!/usr/bin/env python
""" Measurement class.

Holds information about channels in a measurement and builds a regularised
measurement.
"""

from __future__ import absolute_import

import logging
import mplog
import multiprocessing as mp
import os
import re

import ROOT

from utils import *
from AbsMeasurement import AbsMeasurement
from Channel import Channel


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'Measurement'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class Measurement(AbsMeasurement):
    def __init__(self, name, path=None, wsname=None, mcname=None, dataname=None):
        AbsMeasurement.__init__(self, name, path, wsname, mcname, dataname)
        self._filter = "."
        self._channels = dict()

        self._rebin = False
        self._nbins = None
        self._binnedTag = ""
        self._binnedCategories = ""
        self._unbinnedCategories = ""
        self._rebinwWeightName = ""

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Created measurement " + self._name)
        existing_logger.info(msg)

    @property
    def renamings(self):
        return self._renamings

    @renamings.setter
    def renamings(self, renamings):
        self._renamings = renamings

    @property
    def filter(self):
        return self._filter

    @filter.setter
    def filter(self, filter):
        self._filter = filter

    @property
    def channels(self):
        return self._channels

    @channels.setter
    def channels(self, channels):
        self._channels = channels

    def SetDatasetBinning(self, nbins, binnedTag, binnedCategories, unbinnedCategories, rebinwWeightName):
        self._rebin = True
        self._nbins = nbins
        self._binnedTag = binnedTag
        self._binnedCategories = binnedCategories
        self._unbinnedCategories = unbinnedCategories
        self._rebinwWeightName = rebinwWeightName

    def initialise(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Initialising measurement")
        existing_logger.info(msg)

        self._file = ROOT.TFile.Open(self._path)
        self._workspace = self._file.Get(self._wsname)

        # Activate binned likelihood for faster evaluations of RooRealSumPdf
        if self._binnedLikelihood is True:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Activating binned likelihood")
            existing_logger.info(msg)
            iter = self._workspace.components().fwdIterator()
            arg = iter.next()
            while arg:
                if arg.IsA() is ROOT.RooRealSumPdf.Class():
                    arg.setAttribute("BinnedLikelihood")
                    msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Activating binned likelihood for pdf " + arg.GetName())
                    existing_logger.debug(msg)
                arg = iter.next()

        # TODO(StarMomentMorph) Fix cache of RooStarMomentMorph

        self._modelconfig = self._workspace.obj(self._mcname)
        self._data = self._workspace.data(self._dataname)

        # Grab all needed information: categories, (global) observables after editing the workspace and find top-level RooSimultaneous, which is the physics model
        self._pdf = self._modelconfig.GetPdf()
        thisSim = ROOT.RooSimultaneous
        if self._pdf.IsA() is ROOT.RooSimultaneous.Class():
            thisSim = self._pdf
        elif self._pdf.IsA() is ROOT.RooProdPdf.Class():
            pdfList = self._pdf.pdfList()
            iter = pdfList.createIterator()
            arg = iter.Next()
            while arg:
                if arg.IsA() is ROOT.RooSimultaneous.Class():
                    thisSim = arg
                    thisSim.SetName(self._pdf.GetName())
                    thisSim.SetTitle(self._pdf.GetTitle())
                    break
                arg = iter.Next()
        else:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "No RooSimultaneous found")
            existing_logger.warning(msg)

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Identified " + thisSim.GetName() + " as the top level RooSimultaneous")
        existing_logger.info(msg)

        if self._rebin:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Rebinning dataset " + self._data.GetName())
            existing_logger.warning(msg)
            self._data = RebinDataset(thisSim, self._data, self._nbins, self._binnedTag, self._binnedCategories, self._unbinnedCategories, self._rebinwWeightName)

        cat = thisSim.indexCat()

        self._dataList = self._data.split(cat, True)
        self._obs = self._modelconfig.GetObservables()
        self._globs = self._modelconfig.GetGlobalObservables()
        self._nuis = self._modelconfig.GetNuisanceParameters()

        # Fix for handling RooDataHist
        if self._data.IsA() is ROOT.RooDataHist.Class():
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, self._data.GetName() + " is a RooDataHist. Transform it to RooDataSet")
            existing_logger.warning(msg)
            self._data = DataHist2DataSet(thisSim, self._data)

        # Add ghost events
        if self._ghostEvents:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Adding ghost events to dataset " + self._data.GetName())
            existing_logger.warning(msg)
            self._data = AddGhostEvents(self._data, thisSim, self._obs, self._modelconfig.GetParametersOfInterest().first())

        # Find all constraint terms for later usage in regularisation
        # Copies, to keep original sets in place after getAllconstraints call
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Identifying all constraint terms and split possible products")
        existing_logger.info(msg)
        tmpAllNuisanceParameters2 = self._nuis
        tmpAllObservables2 = self._obs
        AllConstraints = self._pdf.getAllConstraints(tmpAllObservables2, tmpAllNuisanceParameters2, False)

        # Take care of products of constraint terms
        self._constraints = ROOT.RooArgSet(AllConstraints.GetName())
        constraintIter = AllConstraints.createIterator()
        nextConstraint = constraintIter.Next()
        while nextConstraint:
            if nextConstraint.IsA() is ROOT.RooProdPdf.Class():
                thisComponents = ROOT.RooArgSet()
                FindUniqueProdComponents(nextConstraint, thisComponents)
                self._constraints.add(thisComponents)
            else:
                self._constraints.add(nextConstraint)
            nextConstraint = constraintIter.Next()

        self._pdf = thisSim
        self._isinit = True

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Initilisation successful")
        existing_logger.info(msg)

    def RegulariseChannel(self, f):
        sub_logger = logging.getLogger('sub')

        ds = f[0]
        thisSim = f[1]
        return_dict = f[2]

        label = ds.GetName()
        thisChannelName = label + "_" + self._name

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Preparing channel " + label + " for regularisation")
        sub_logger.info(msg)

        # Check if the channel passes the specified filter or if it should be dropped
        searchObj = re.search(self._filter, label)
        if searchObj:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Channel " + label + " passes filter")
            sub_logger.debug(msg)
        else:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Channel " + label + " fails filter - skip it")
            sub_logger.warning(msg)
            return

        # Weight the dataset in case it does not contain weights to avoid a mixture of weighted and unweighted categories which can confuse RooFit
        if not ds.isWeighted():
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Dataset " + label + " is not weighted - adding weights 1")
            sub_logger.warning(msg)
            weightVar = ROOT.RooRealVar("weightVar", "", 1.0, -1e10, 1e10)
            ds.addColumn(weightVar)
            obs_cat_weight = ROOT.RooArgSet()
            obs_cat_weight.add(self._obs)
            obs_cat_weight.add(weightVar)
            tmpDs = ROOT.RooDataSet(ds.GetName(), ds.GetTitle(), obs_cat_weight, ROOT.RooFit.Import(ds), ROOT.RooFit.WeightVar("weightVar"))
            ds = tmpDs

        # Get the pdf that is associated with this channel
        thisPdf = thisSim.getPdf(label)
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Identified " + thisPdf.GetName() + " as physics pdf")
        sub_logger.info(msg)
        NuisanceParametersPlusPOIs = self._modelconfig.GetNuisanceParameters()
        NuisanceParametersPlusPOIs.add(self._modelconfig.GetParametersOfInterest())

        # Find the nuisance parameters that are associated with this channel
        thistmpNuisanceParameters = ROOT.RooArgSet()
        nuisIter = self._nuis.createIterator()
        nextNuisanceParameter = nuisIter.Next()
        while nextNuisanceParameter:
            if thisPdf.dependsOn(nextNuisanceParameter):
                thistmpNuisanceParameters.add(nextNuisanceParameter)
            nextNuisanceParameter = nuisIter.Next()

        # Build a new pdf which is the product of the physics pdf and the constraint terms of the nuisance parameters associated with this channel.
        tmpComp = ROOT.RooArgList()
        tmpComp.add(thisPdf)
        constraintIter = self._constraints.createIterator()
        nextConstraint = constraintIter.Next()
        while nextConstraint:
            nuisIter = thistmpNuisanceParameters.createIterator()
            nextNuisanceParameter = nuisIter.Next()
            while nextNuisanceParameter:
                if nextConstraint.dependsOn(nextNuisanceParameter):
                    tmpComp.add(nextConstraint)
                    break
                nextNuisanceParameter = nuisIter.Next()
            nextConstraint = constraintIter.Next()

        prodPdf = ROOT.RooProdPdf(thisPdf.GetName(), thisPdf.GetTitle(), tmpComp)

        thisComponents = ROOT.RooArgList()
        FindUniqueProdComponents(prodPdf, thisComponents)

        thisProdPdf = ROOT.RooProdPdf(thisPdf.GetName() + "_complete", thisPdf.GetTitle() + "_complete", thisComponents)

        # Perform the actual regularisation of the channel
        renamings = self._renamings.copy()
        globs = ROOT.RooArgSet(self._globs)
        obs = ROOT.RooArgSet(self._obs)
        thisChannel = Channel(thisChannelName, thisProdPdf, ds, self._name)
        thisChannel.renamings = renamings
        thisChannel.globs = globs
        thisChannel.nuis = NuisanceParametersPlusPOIs
        thisChannel.obs = obs
        thisChannel.Regularise()
        return_dict[thisChannelName] = thisChannel

    def CollectChannels(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Collecting channels")
        existing_logger.info(msg)
        with mplog.open_queue() as log_queue:
            if self._isinit is False:
                existing_logger.info('Require the measurement to be initialised.')
                self.initialise()

            thisSim = self._pdf
            nrEntries = self._dataList.GetEntries()

            # TODO(Attributes) Add missing attributes to the renaming map

            # Steer the regularisation
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Distributing regularisation over " + repr(self._nrCores) + " cores")
            existing_logger.info(msg)
            overallCounter = 0
            manager = mp.Manager()
            return_dict = manager.dict()
            for i in xrange(0, int(nrEntries / self._nrCores) + (nrEntries % self._nrCores > 0)):
                procs = list()
                for itrChan in xrange(overallCounter, overallCounter + self._nrCores):
                    ds = self._dataList.At(itrChan)
                    f = (ds, thisSim, return_dict)
                    proc = mp.Process(target=mplog.logged_call, args=(log_queue, self.RegulariseChannel, f))
                    proc.start()
                    procs.append(proc)

                    overallCounter += 1
                    if overallCounter >= nrEntries:
                        break

                for proc in procs:
                    proc.join()

            # Harvest the regularied parameters
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Harvesting regularised channels")
            existing_logger.info(msg)
            tmpNuisanceParameters = ROOT.RooArgSet("NuisanceParameters")
            tmpObservables = ROOT.RooArgSet("Observables")
            tmpGlobalObservables = ROOT.RooArgSet("GlobalObservables")

            for label in return_dict.keys():
                channel = return_dict[label]
                self._channels[label] = channel
                thisObservables = channel.obs
                thisGlobalObservables = channel.globs
                thisNuisanceParameters = channel.nuis

                tmpObservables.add(thisObservables)
                tmpGlobalObservables.add(thisGlobalObservables)
                tmpNuisanceParameters.add(thisNuisanceParameters)

            self._obs = tmpObservables
            self._globs = tmpGlobalObservables
            self._nuis = tmpNuisanceParameters
            self._globs.setAttribAll("GLOBAL_OBSERVABLE")

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "All channels collected")
        existing_logger.info(msg)


if __name__ == '__main__':
    pass
