#!/usr/bin/env python
""" Channel regularisation.

Executes regularisation on the channel level, e.g. renaming parameters.
"""

from __future__ import absolute_import

import logging
import os

import ROOT


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'Channel'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class Channel(object):
    def __init__(self, name, pdf, data, parent):
        self._name = name
        self._pdf = pdf
        self._data = data
        self._parent = parent
        self._ws = ROOT.RooWorkspace(name)

        sub_logger = logging.getLogger('sub')
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Created channel " + self._name)
        sub_logger.info(msg)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def pdf(self):
        return self._pdf

    @pdf.setter
    def pdf(self, pdf):
        self._pdf = pdf

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @property
    def nuis(self):
        return self._nuis

    @nuis.setter
    def nuis(self, nuis):
        self._nuis = nuis

    @property
    def obs(self):
        return self._obs

    @obs.setter
    def obs(self, obs):
        self._obs = obs

    @property
    def globs(self):
        return self._globs

    @globs.setter
    def globs(self, globs):
        self._globs = globs

    @property
    def renamings(self):
        return self._renamings

    @renamings.setter
    def renamings(self, renamings):
        self._renamings = renamings

    def Regularise(self):
        sub_logger = logging.getLogger('sub')
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Starting regularisation")
        sub_logger.info(msg)
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
        getattr(self._ws, 'import')(self._pdf, ROOT.RooFit.RenameAllNodes(self._parent), ROOT.RooFit.RenameAllVariables(self._parent), ROOT.RooFit.Silence())
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.DEBUG)

        tmpGlobalObservables = ROOT.RooArgSet("GlobalObservables")
        tmpObservables = ROOT.RooArgSet("Observables")
        tmpNuisanceParameters = ROOT.RooArgSet("NuisanceParameters")

        tmpAllNuisanceParameters2 = self._nuis
        tmpAllObservables2 = self._obs
        tmpAllConstraints = self._pdf.getAllConstraints(tmpAllObservables2, tmpAllNuisanceParameters2, False)

        for old in self._renamings:
            oldName = old + "_" + self._parent
            if self._ws.var(oldName):
                foundConstraint = False
                constraintIter = tmpAllConstraints.createIterator()
                nextConstraint = constraintIter.Next()
                while nextConstraint and not foundConstraint:
                    thisImportedConstraint = self._ws.pdf(nextConstraint.GetName() + "_" + self._parent)
                    if thisImportedConstraint.dependsOn(self._ws.var(oldName)):
                        foundConstraint = True
                        oldConstraintName = nextConstraint.GetName()
                        self._renamings[old]['constraint'] = oldConstraintName

                        foundGlobalObservable = False
                        globsIter = self._globs.createIterator()
                        nextGlobalObservable = globsIter.Next()
                        while nextGlobalObservable and not foundGlobalObservable:
                            if nextConstraint.dependsOn(nextGlobalObservable):
                                foundGlobalObservable = True
                                oldGlobName = nextGlobalObservable.GetName()
                                self._renamings[old]['glob'] = oldGlobName
                            nextGlobalObservable = globsIter.Next()
                    nextConstraint = constraintIter.Next()

        for old in self._renamings:
            oldName = old + "_" + self._parent
            newName = self._renamings[old]['new']
            if self._ws.var(oldName):
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Renaming (nuisance) parameter " + old + " to " + newName)
                sub_logger.debug(msg)
                self._ws.var(oldName).SetName(newName)

                if 'glob' in self._renamings[old]:
                    oldGlobName = self._renamings[old]['glob']
                    newGlobName = newName + "In"
                    if self._ws.var(oldGlobName + "_" + self._parent):
                        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Renaming global observable " + oldGlobName + " to " + newGlobName)
                        sub_logger.debug(msg)
                        self._ws.var(oldGlobName + "_" + self._parent).SetName(newGlobName)

                if 'constraint' in self._renamings[old]:
                    oldConstraintName = self._renamings[old]['constraint']
                    newConstraintName = newName + "_pdf"
                    if self._ws.obj(oldConstraintName + "_" + self._parent):
                        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Renaming constraint " + oldConstraintName + " to " + newConstraintName)
                        sub_logger.debug(msg)
                        self._ws.pdf(oldConstraintName + "_" + self._parent).SetName(newConstraintName)

        nuisIter = self._nuis.createIterator()
        nextNuisanceParameter = nuisIter.Next()
        while nextNuisanceParameter:
            if self._pdf.dependsOn(nextNuisanceParameter):
                if nextNuisanceParameter.GetName() in self._renamings:
                    tmpNuisanceParameters.add(self._ws.var(self._renamings[nextNuisanceParameter.GetName()]['new']))
                else:
                    tmpNuisanceParameters.add(self._ws.var(nextNuisanceParameter.GetName() + "_" + self._parent))
            nextNuisanceParameter = nuisIter.Next()
        self._nuis = tmpNuisanceParameters

        observablesFromData = self._data.get()
        obsIter = observablesFromData.createIterator()
        nextObservable = obsIter.Next()
        while nextObservable:
            nameBeforeImport = nextObservable.GetName()
            nameAfterImport = nameBeforeImport + "_" + self._parent
            if self._ws.var(nameAfterImport):
                self._ws.var(nameAfterImport).SetName(nameBeforeImport)
                tmpObservables.add(self._ws.var(nameBeforeImport))
            nextObservable = obsIter.Next()
        self._obs = tmpObservables

        globsIter = self._globs.createIterator()
        nextGlobalObservable = globsIter.Next()
        while nextGlobalObservable:
            newGlobName = nextGlobalObservable.GetName() + "_" + self._parent
            for nuis, attributes in self._renamings.iteritems():
                if 'glob' in attributes and self._ws.var(attributes['new']) and nextGlobalObservable.GetName() == attributes['glob']:
                    newGlobName = attributes['new'] + "In"
                    break
            if self._ws.var(newGlobName):
                tmpGlobalObservables.add(self._ws.var(newGlobName))
            nextGlobalObservable = globsIter.Next()
        self._globs = tmpGlobalObservables
        self._globs.setAttribAll("GLOBAL_OBSERVABLE")

        self._pdf = self._ws.pdf(self._pdf.GetName() + "_" + self._parent)


if __name__ == '__main__':
    pass
